function decollage(){
    // console.log("prepare for lift off...");
    // for(let i = 10; i > 0; i--){
    //     console.log(i+"...")
    // }
    // console.log("lift off!")
    let fullRocket = document.createElement("div");
    fullRocket.classList.add("rocket");

    let rocket = document.createElement("img");
    rocket.src = "/static/decollage/rocket.svg";

    let fire = document.createElement("img");
    fire.src = "/static/decollage/fire1.svg";
    fire.id = "rocketfire";

    fullRocket.appendChild(rocket);
    fullRocket.appendChild(fire);
    document.body.appendChild(fullRocket);
    window.setInterval(()=>{fires()}, 100);
}

function fires() {
    let fire = document.getElementById("rocketfire");
    fire.src = Math.random() > 0.5 ? "/static/decollage/fire1.svg" : "/static/decollage/fire2.svg";
    let r = fire.getBoundingClientRect();
    console.log(r.top, r.right, r.bottom, r.left);
}