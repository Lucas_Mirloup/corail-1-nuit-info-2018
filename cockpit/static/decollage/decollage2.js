let rocketDirection = 0;
let rocketPos = [0, 0];
let particleList = Array();

function initRocket(){
    let fullRocket = document.createElement("div");
    fullRocket.id = "rocket";

    let rocket = document.createElement("img");
    rocket.src = "/static/decollage/rocket.svg";

    let fire = document.createElement("img");
    fire.src = "/static/decollage/fire1.svg";
    fire.id = "rocketfire";

    fullRocket.appendChild(rocket);
    fullRocket.appendChild(fire);

    let countdown = document.createElement("div");
    countdown.id = "countdown";
    let cd = document.createElement("h1");
    countdown.appendChild(cd);
    cd.innerText = "3";

    let particles = document.createElement("div");
    particles.id = "particles";

    document.body.appendChild(fullRocket);
    document.body.appendChild(countdown);
    document.body.appendChild(particles);

    window.setInterval(()=>{fireRocket()}, 100);
    window.setInterval(()=>{updateParticles()}, 10);

    window.setTimeout(()=>{
        window.setTimeout(()=>{
            cd.innerText = "2";
            window.setTimeout(()=>{
                cd.innerText = "1";
                window.setTimeout(()=>{
                    countdown.remove();
                    document.onmousemove = (event) => {
                        let rocket = document.getElementById("rocket");
                        rocketDirection = Math.atan2((event.clientX-rocket.offsetLeft),(rocket.offsetTop-event.clientY));
                    };
                    window.setInterval(()=>{updateRocketDirection()}, 10);
                    window.setInterval(()=>{updateRocketPosition()}, 10);
                }, 1000)
            }, 1000)
        }, 1000)
    }, 1000);
}

function fireRocket() {
    let fire = document.getElementById("rocketfire");
    fire.src = Math.random() > 0.5 ? "/static/decollage/fire1.svg" : "/static/decollage/fire2.svg";
    let r = fire.getBoundingClientRect();

    Math.random() > 0.9 ? newParticle() : null;
}

function updateRocketDirection(){
    let rocket = document.getElementById("rocket");
    rocket.style.transform = "rotate("+rocketDirection+"rad)";
}

function updateRocketPosition(){
    rocketPos[0] = rocketPos[0]+Math.sin(rocketDirection)*3;
    rocketPos[1] = rocketPos[1]-Math.cos(rocketDirection)*3;

    let rocket = document.getElementById("rocket");
    rocket.style.left = rocketPos[0] + "px";
    rocket.style.top = rocketPos[1] + "px";
}

function newParticle(){
    let particle = document.createElement("img");
    particle.classList.add("rocket-particle");
    particle.src = "/static/decollage/puff.png";
    particle.style.left = rocketPos[0] + "px";
    particle.style.top = rocketPos[1] + "px";
    document.getElementById("particles").appendChild(particle);
    particleList.push(particle);
}

function updateParticles(){
    for(let particle of particleList){
        let pos = parseFloat(particle.style.top.slice(0,-2)) + 2;
        particle.style.top = pos + "px";
    }
}