class CactusManager{
    constructor(){
      this.cactusList = [];
      this.nbupdate = 0;
    }

    update(){
      if(this.nbupdate == 40){
        this.nbupdate = 0;
        if(Math.random() > 0.3){
          this.cactus = new PIXI.Sprite(PIXI.loader.resources["assets/cactus.png"].texture);
          this.cactus.anchor.set(0.5, 0.5);

          // Diversify cactus size
          let minScale = 0.3;
          let maxScale = 0.8;
          let scale = Math.random() * (maxScale - minScale) + minScale;
          this.cactus.scale.set(scale, scale);
          this.cactus.position.set(renderer.width, renderer.height - 50 * scale);


          stage.addChildAt(this.cactus, 0);
          this.cactusList.push(this.cactus);
        }
      }
      this.nbupdate ++;

      this.cactusList.forEach(function(element, index, array) {
          element.position.x -= 6;

          if (element.position.x < -renderer.width * 0.3) {
              element.destroy();
              array.splice(0, 1);
              score ++;
              document.getElementById("score").innerHTML="Score : "+score;
          }
      });
    }

   colide(){
     this.cactusList.forEach(function(element, index, array) {
     if (element.position.x == explorer.sprite.position.x && explorer.sprite.position.y > element.position.y-80) {
       dead = true;
     }
     });
   }
}
