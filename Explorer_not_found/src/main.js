/*****************************************************

                    Main.js

*****************************************************/

var stage;
var explorer;
var dead;
var score;
var max_score = 0;
PIXI.loader.add(["assets/cactus.png",
                 "assets/explorer.png",
                 "assets/desert.png"
                 ]).load(init);

function init(){
    stage = new PIXI.Container();
    score = 0;
    dead = false;
    document.getElementById("score").innerHTML="Score : 0";
    document.getElementById("score").hidden = true;
    document.getElementById("death").hidden = true;
    document.getElementById("retry").hidden = true;
    renderer.backgroundColor = 0x22A7F0;
    renderer.render(stage);
    explorer = new Explorer();
    cactusManager = new CactusManager();
    loop();
}

function loop(){
  if(!dead){
    requestAnimationFrame(loop);
    renderer.render(stage);
    explorer.update();
    cactusManager.update();
    cactusManager.colide();
  }
  else {
    if(score > max_score){
      max_score = score;
      document.getElementById("max_score").innerHTML="Meilleur Score : "+max_score;
    }
    document.getElementById("death").hidden = false;
    document.getElementById("retry").hidden = false;
  }
}
