/*
** Explorer.js
** Functions used to define the explorer object
**
*/

class Explorer
{
    constructor()
    {
        this.sprite = new PIXI.Sprite(PIXI.loader.resources["assets/explorer.png"].texture);


        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.position.set(100, renderer.height - 60 );
        this.sprite.scale.set(0.2, 0.2);

        window.addEventListener('keydown', this.onKeyDown.bind(this));
        this.Yvit = 7;
        this.is_jumping = false;
        this.percentjump = 0;
        stage.addChild(this.sprite);

    };

    onKeyDown(key){
        if (key.keyCode == 38 && this.is_jumping == false){
            this.is_jumping = true;
            this.percentjump = 0;
        }
    };

    jump(){
    if (this.is_jumping == true){
        let percentjumping = 13;
        if (this.percentjump < percentjumping){
            this.sprite.position.y -= this.Yvit;
        } else {
            this.sprite.position.y += this.Yvit;
            if (this.percentjump > 2*percentjumping - 2) {
                this.is_jumping = false;
            };
        };
        this.percentjump++;
        }
    };

    update(){
        this.jump();
    };
}
